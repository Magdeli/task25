﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task24.Models;

namespace Task24.Controllers
{
    // Magdeli Holmøy Asplin
    // 9/2/2019

    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            // In this method the current date and time ar printed out from the viewbag, and four supervisors are 
            // added to the list of supervisors manually

            ViewBag.DateandTime = DateTime.Now;

            SupervisorGroup.AddSup(new Supervisor { Name = "Per", Id = 1 });
            SupervisorGroup.AddSup(new Supervisor { Name = "Paal", Id = 2 });
            SupervisorGroup.AddSup(new Supervisor { Name = "Espen", Id = 3 });
            SupervisorGroup.AddSup(new Supervisor { Name = "Jim", Id = 4 });

            return View("MyFirstView");
        }

        [HttpGet]
        public IActionResult SupInfo()
        {
            // This method only returns the SupInfo view, ignoring what was done in task 24

            //// Creating a list of supervisors that can be displayed in view
            //List<Supervisor> Supervisors = new List<Supervisor>();

            //// Adding supervisors to the list
            //Supervisors.Add(new Supervisor { Name = "Per", Id = 1 });
            //Supervisors.Add(new Supervisor { Name = "Paal", Id = 2 });
            //Supervisors.Add(new Supervisor { Name = "Espen", Id = 3 });

            //return View(Supervisors);

            return View();
        }

        [HttpPost]
        public IActionResult SupInfo(Supervisor supervisor)
        {
            // This method gets input from the user and stores it in the temporary list of supervisors, it also checks if the
            // input is valid

            if (ModelState.IsValid)
            {
                SupervisorGroup.Supervisors.Add(supervisor);
                return View("AddSupConfirmation", supervisor);
            }
            else
            {
                return View();
            }
            
        }

        [HttpGet]
        public IActionResult AllSupervisors(Supervisor supervisor)
        {
            // This method makes it possible for the AllSupervisors view to display the list of supervisors

            return View(SupervisorGroup.Supervisors);
        }
    }
}
