﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task24.Models
{
    public static class SupervisorGroup
    {
        // Magdeli Holmøy Asplin
        // 9/2/2019

        // This class contains a temporary list of supervisors

        #region Attributes
        // Only one attribute; a list of supervisors

        private static List<Supervisor> CurrentSupervisors = new List<Supervisor>();

        #endregion

        #region Behaviours

        public static List<Supervisor> Supervisors
        {
            // This method returns the current temporary list of supervisors
            get { return CurrentSupervisors; }
        }

        public static void AddSup(Supervisor newSupervisor)
        {
            // This method adds a supervisor to the current list of supervisors
            CurrentSupervisors.Add(newSupervisor);
        }

        #endregion

    }
}
