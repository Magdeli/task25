﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Task24.Models
{
    // Magdeli Holmøy Asplin
    // 9/2/2019

    // This class can be used to create a supervisor with a name and an id
    public class Supervisor
    {
        [Required(ErrorMessage ="Please enter a number")]
        [RegularExpression("^\\d+$", ErrorMessage ="Please enter a valid whole number")]
        public int Id { get; set; }

        [Required(ErrorMessage ="Please enter a name")]
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
